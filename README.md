# Bot for search flat in Minsk using onliner.by and kvartirant.by

## Install requirements:
1. virtualenv venv -p python3
2. source venv/bin/activate
3. pip install -r requirements.txt
4. fill `settings.json` file from `settings.json.example`

## Initialize db and run test:
1. python db.py
2. python initialize_db.py
3. python test.py

## Run scheduler:
1. python scheduler.py