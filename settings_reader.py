import json

FILE_NAME = 'settings.json'


def read():
    json_data = {}
    with open(FILE_NAME) as f:
        json_data = json.load(f)

    return json_data


def read_facebook_settings():
    json_data = read()
    return json_data['facebook']


def read_email_settings():
    json_data = read()
    return json_data['email']


def read_telegram_settings():
    json_data = read()
    return json_data['telegram']
