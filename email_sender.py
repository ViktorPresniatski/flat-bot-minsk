import smtplib
from settings_reader import read_email_settings


settings = read_email_settings()
SMTP = 'smtp.mail.ru'
SENDER = settings.get('email')
PWD = settings.get('password')
RECIEVERS = settings.get('receivers')
SERVER = smtplib.SMTP(SMTP, 25)


class EmailSender:

    @classmethod
    def send_notification(cls, flat_list):
        SERVER.connect(SMTP, 587)
        SERVER.ehlo()
        SERVER.starttls()
        SERVER.ehlo()
        login = SERVER.login(SENDER, PWD)
        print(login)
        message = cls.make_message(flat_list)
        response = SERVER.sendmail(SENDER, RECIEVERS[0], message)
        return flat_list

    @classmethod
    def make_message(cls, flat_list):
        text = ''
        for flat in flat_list:
            for k, v in flat.items():
                text += f'{k}: {v}\n'
            text += '\n'

        return text
