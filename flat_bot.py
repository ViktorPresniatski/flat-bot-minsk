import json
import requests
import xmltodict

from datetime import timedelta
import dateutil.parser as datetime_parser
from db import Flat


f = open('search_params.json')
PARAMS = json.load(f)
f.close()


class FlatBot:

    def initialize_db(self):
        data = self.search()
        data.sort(key=lambda x: self._get_publish_time(x), reverse=True)
        record = data[0]
        params = self.make_params(record)
        params['updated_at'] -= timedelta(hours=2)
        Flat.create(**params)

    def get_new_flats(self):
        flat = Flat.select().where(Flat.source == self.SOURCE).order_by(Flat.updated_at.desc()).get()
        data = self.search()
        comparator = lambda x: self._get_publish_time(x) > flat.get_updated_at()
        data.sort(key=lambda x: self._get_publish_time(x))
        new_flats = [item for item in data if comparator(item)]
        return self.get_flats_fields(new_flats)

    def search(self):
        response = requests.get(url=self.SEARCH_URL, params=self.PARAMS)

        if not response.ok:
            raise Exception("FAILED RESPONSE")

        return self.parse_response(response)

    def get_flats_fields(self, flats):
        return [self.make_params(flat) for flat in flats]

    def make_params(self, flat):
        return {
            'source': self.SOURCE,
            'updated_at': self._get_publish_time(flat),
            'price': self._get_price(flat),
            'url': self._get_url(flat),
        }


class OnlinerFlatBot(FlatBot):
    SOURCE = Flat.ONLINER
    SEARCH_URL = 'https://ak.api.onliner.by/search/apartments'
    PARAMS = PARAMS['data'][SOURCE]

    def parse_response(self, response):
        return response.json()['apartments']

    def _get_publish_time(self, flat):
        return datetime_parser.parse(flat['last_time_up'])

    def _get_price(self, flat):
        return flat.get('price', {}).get('amount')

    def _get_url(self, flat):
        return flat.get('url')


class KvartirantFlatBot(FlatBot):
    SOURCE = Flat.KVARTIRANT
    SEARCH_URL = 'https://www.kvartirant.by/ads/flats/type/rent/rss.xml'
    PARAMS = PARAMS['data'][SOURCE]

    def parse_response(self, response):
        json_data = xmltodict.parse(response.text)
        return json_data['rss']['channel']['item']

    def _get_publish_time(self, flat):
        return datetime_parser.parse(flat['pubDate'])

    def _get_price(self, flat):
        return ''

    def _get_url(self, flat):
        return flat.get('link')
