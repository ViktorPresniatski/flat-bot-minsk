import telegram
from settings_reader import read_telegram_settings


settings = read_telegram_settings()

TOKEN = settings.get('token')
RECEIVERS = settings.get('receivers')
bot = telegram.Bot(token=TOKEN)


class TelegramSender:

    @classmethod
    def send_notification(cls, flat_list):
        message = cls.make_message(flat_list)
        ret = []
        for receiver in RECEIVERS:
            try:
                bot.send_message(chat_id=receiver, text=message)
                ret.append(True)
            except Exception:
                ret.append(False)

        return all(ret)

    @classmethod
    def make_message(cls, flat_list):
        text = ''
        for flat in flat_list:
            for k, v in flat.items():
                text += f'{k}: {v}\n'
            text += '\n'

        return text
