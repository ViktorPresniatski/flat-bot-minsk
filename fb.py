from fbchat import Client, Message, ThreadType
from settings_reader import read_facebook_settings


settings = read_facebook_settings()
RECIEVERS = settings.get('receivers')
USERNAME = settings.get('username')
PWD = settings.get('password')


class FacebookSender:

    @classmethod
    def send_notification(cls, flat_list):
        ret = []
        client = Client(USERNAME, PWD)
        for flat in flat_list:
            message = cls.make_message(flat)
            for receiver in RECIEVERS:
                response = client.send(message, thread_id=receiver, thread_type=ThreadType.USER)

            if response:
                ret.append(flat)

        client.logout()
        return ret

    @classmethod
    def make_message(cls, flat):
        text = ''
        for k, v in flat.items():
            text += f'{k}: {v}\n'

        return Message(text=text)
