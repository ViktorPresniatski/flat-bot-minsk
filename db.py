from peewee import *
import dateutil.parser as datetime_parser


db = SqliteDatabase('sqlite.db')


class Flat(Model):
    ONLINER = 'onliner'
    KVARTIRANT = 'kvartirant'
    SOURCE_CHOICES = (
        (ONLINER, 'Onliner'),
        (KVARTIRANT, 'Kvartirant')
    )

    source = CharField()
    updated_at = DateTimeField()
    url = CharField(null=True)
    price = CharField(choices=SOURCE_CHOICES, null=True)

    class Meta:
        database = db
        order_by = ['updated_at']

    def get_updated_at(self):
        return datetime_parser.parse(self.updated_at)


if __name__ == '__main__':
    Flat.drop_table()
    Flat.create_table()
