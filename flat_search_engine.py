from db import Flat
from flat_bot import OnlinerFlatBot, KvartirantFlatBot
from telegram_sender import TelegramSender
# from fb import FacebookSender
# from email_sender import EmailSender


BOT_MAPPER = {
    Flat.ONLINER: OnlinerFlatBot(),
    Flat.KVARTIRANT: KvartirantFlatBot(),
}


class FlatSearchEngine:

    @classmethod
    def run(cls):
        new_flats = []
        sources = [source[0] for source in Flat.SOURCE_CHOICES]
        for source in sources:
            try:
                flats = BOT_MAPPER[source].get_new_flats()
                new_flats.extend(flats)
            except Exception:
                print('BAD REQUEST')

        if not new_flats:
            return

        response = cls.send_notification(new_flats)
        if not response:
            print("Telegram was failed")

        Flat.insert_many(new_flats).execute()
        print(f"Sent {len(new_flats)} flats")
        print(f"Sent {[flat['url'] for flat in new_flats]} flats")

    @classmethod
    def send_notification(cls, new_flats):
        # try:
            # return FacebookSender.send_notification(new_flats)
        return TelegramSender.send_notification(new_flats)
        # except Exception:
            # cls.console_log(new_flats)

    @classmethod
    def console_log(cls, new_flats):
        for flat in new_flats:
            print(flat['url'])
