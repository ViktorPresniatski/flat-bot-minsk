from flat_bot import OnlinerFlatBot, KvartirantFlatBot
from db import Flat


BOT_MAPPER = {
    Flat.ONLINER: OnlinerFlatBot(),
    Flat.KVARTIRANT: KvartirantFlatBot(),
}


def initialize_db():
    sources = [source[0] for source in Flat.SOURCE_CHOICES]
    for source in sources:
        BOT_MAPPER[source].initialize_db()


if __name__ == '__main__':
    initialize_db()
