import time
import schedule
from pytz import timezone
from datetime import datetime

from flat_search_engine import FlatSearchEngine


def time_control(func):
    def wrapped():
        tz = timezone('Europe/Minsk')
        now = datetime.now().astimezone(tz)
        print(f"Start job at - {now}")
        func()
        now = datetime.now().astimezone(tz)
        print(f"End job at - {now}\n")

    return wrapped


@time_control
def job():
    FlatSearchEngine.run()


schedule.every(0.5).minutes.do(job)

while True:
    schedule.run_pending()
    time.sleep(1)
